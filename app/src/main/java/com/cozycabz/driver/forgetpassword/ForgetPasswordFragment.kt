package com.cozycabz.driver.forgetpassword


import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.cozycabz.driver.R
import com.cozycabz.driver.di.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_forget_password.view.*

/**
 * A simple [Fragment] subclass.
 */
class ForgetPasswordFragment : BaseFragment() {
    override fun getFragLayoutId(): Int = R.layout.fragment_forget_password
    override fun afterViewCreated(view: View, savedInstanceState: Bundle?) {
        view.back.setOnClickListener {
        }

    }


}
