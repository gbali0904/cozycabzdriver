package com.cozycabz.driver.di.module

import android.content.Context
import com.cozycabz.driver.DriverApplication

import dagger.Module
import dagger.Provides

@Module
class ApplicationModule {

    @Provides
    internal fun provideContext(context: DriverApplication): Context {
        return context
    }

}
