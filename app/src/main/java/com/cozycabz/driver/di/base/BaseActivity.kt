package com.cozycabz.driver.di.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.lifecycle.ViewModelProvider
import com.cozycabz.driver.R
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

abstract class BaseActivity : DaggerAppCompatActivity() {

    @get:LayoutRes
    private var layoutId = R.layout.activity_base


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
    }

    override fun onBackPressed() {
        supportFragmentManager?.let {
            if (it.backStackEntryCount <= 1) {
                finish()
                return
            }
            super.onBackPressed()
        }
    }

}