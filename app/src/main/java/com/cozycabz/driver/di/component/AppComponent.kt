package com.cozycabz.driver.di.component

import com.cozycabz.driver.DriverApplication
import com.cozycabz.driver.di.DriverModule
import com.cozycabz.driver.di.module.ActivityModule
import com.cozycabz.driver.di.module.AppModule
import com.cozycabz.driver.di.module.ApplicationModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, ApplicationModule::class, AppModule::class, ActivityModule::class ,DriverModule::class])
interface AppComponent : AndroidInjector<DriverApplication> {
    @Component.Factory
    abstract class Factory : AndroidInjector.Factory<DriverApplication>

}
