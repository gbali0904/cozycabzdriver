package com.cozycabz.driver.di

import com.cozycabz.driver.data.remote.DriverApiInterface
import com.cozycabz.driver.util.Constants
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class DriverModule {

    @Provides
    fun provideGson(): Gson = Gson()

    @Provides
    fun provideApiService(): DriverApiInterface {
        var okHttpBuilder = OkHttpClient.Builder()
        okHttpBuilder.connectTimeout(30, TimeUnit.MILLISECONDS)
            .readTimeout(30, TimeUnit.MILLISECONDS)
            .writeTimeout(30, TimeUnit.MILLISECONDS)
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpBuilder.build())
            .build()
            .create(DriverApiInterface::class.java!!)

    }


}