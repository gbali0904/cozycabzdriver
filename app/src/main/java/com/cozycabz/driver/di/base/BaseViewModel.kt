package com.cozycabz.driver.di.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.airtel.cocp.data.model.GlobalState
import com.airtel.cocp.data.model.State
import com.cozycabz.driver.data.model.ResponseResult
import com.cozycabz.driver.helper.launchOrError
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel : ViewModel() , CoroutineScope {
    protected val globalStateLiveData: MutableLiveData<GlobalState> by lazy { MutableLiveData<GlobalState>() }

    fun getGlobalStateData() = globalStateLiveData

    override val coroutineContext: CoroutineContext = Dispatchers.Main + SupervisorJob()

    fun <T> updateGlobalState(apiResult: ResponseResult<T>, response: (ResponseResult<T>) -> Unit) {
        when (apiResult) {
            is ResponseResult.Success<T> -> response.invoke(apiResult)
            is ResponseResult.Error -> {
                if (apiResult.code == State.SESSION_EXPIRED.code) {
                    globalStateLiveData.postValue(GlobalState(State.SESSION_EXPIRED))
                } else {
                    response.invoke(apiResult)
                }
            }
        }
    }

    fun executeJob(func: suspend CoroutineScope.() -> Unit): Job {
        return launchOrError(func = func, error = ::handleError)
    }

    //abstract fun handleError(throwable: Throwable)

    private fun handleError(throwable: Throwable){
        globalStateLiveData.postValue(GlobalState(State.ERROR,throwable.message))
    }

    override fun onCleared() {
        cancel("clear view model")
        super.onCleared()
    }




}