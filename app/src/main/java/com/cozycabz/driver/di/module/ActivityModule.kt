package com.cozycabz.driver.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cozycabz.driver.data.remote.repo.LoginRepo
import com.cozycabz.driver.data.remote.repo.LoginRepoImpl
import com.cozycabz.driver.di.ViewModelFactory
import com.cozycabz.driver.di.ViewModelKey
import com.cozycabz.driver.forgetpassword.ForgetPasswordFragment
import com.cozycabz.driver.home.HomeActivity
import com.cozycabz.driver.login.LoginActivity
import com.cozycabz.driver.login.viewmodel.LoginViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class ActivityModule {
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory


    /**
     * Login
     * */
    @ContributesAndroidInjector
    abstract fun provideLoginActvity(): LoginActivity


    @ContributesAndroidInjector
    abstract fun provideforgetPassword(): ForgetPasswordFragment


    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun provideLoginViewModel(loginViewModel: LoginViewModel): ViewModel


    @Binds
    abstract fun provideLoginRepo(loginRepoImpl: LoginRepoImpl): LoginRepo

    /**
     * Home
     * */
    @ContributesAndroidInjector
    abstract fun provideHomeActvity(): HomeActivity

}