package com.cozycabz.driver.util

interface Constants {

    companion object {
        const val PERMISSION_MESSAGE = "Please provide permission for smooth functionality of app."
        const val BASE_URL = ""
    }
}