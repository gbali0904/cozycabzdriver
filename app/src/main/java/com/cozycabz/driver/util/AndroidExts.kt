package com.cozycabz.driver.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity


inline fun <reified T : Any> Activity.launchActivityForResult(
    requestCode: Int,
    options: Bundle? = null,
    noinline init: Intent.() -> Unit = {}
) {
    val intent = newIntent<T>(this)
    intent.init()
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
        startActivityForResult(intent, requestCode, options)
    } else {
        intent.putExtra("bundle", options)
        startActivityForResult(intent, requestCode)
    }
}

inline fun <reified T : Any> Activity.launchActivity(
    options: Bundle? = null,
    init: Intent.() -> Unit = {}
) {
    val intent = newIntent<T>(this)
    intent.init()
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
        startActivity(intent, options)
    } else {
        intent.putExtra("bundle", options)
        startActivity(intent)
    }
}

inline fun <reified T : Any> newIntent(context: Context): Intent =
    Intent(context, T::class.java)

inline fun <reified T : Any> Fragment.launchActivity(
    options: Bundle? = null,
    init: Intent.() -> Unit = {}
) {
    this.activity?.launchActivity<T>(options, init)
}

fun FragmentActivity.replaceFragment(
    container: Int,
    fragment: Fragment,
    isAddToBackStack: Boolean = true,
    frag: Fragment.() -> Unit = {}
) {
    fragment.frag()
    if (isAddToBackStack) {
        this.supportFragmentManager.beginTransaction()
            .addToBackStack(fragment.javaClass.name)
            .replace(container, fragment, fragment.javaClass.name)
            .commit()
    } else {
        this.supportFragmentManager.beginTransaction()
            .replace(container, fragment, fragment.javaClass.name)
            .commit()
    }
}


fun Activity.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Fragment.showToast(message: String) {
    this.activity?.let {
        android.widget.Toast.makeText(it, message, android.widget.Toast.LENGTH_SHORT).show()
    }
}

fun View.visible()= apply {visibility= View.VISIBLE }
fun View.invisible()= apply { visibility= View.INVISIBLE }
fun View.gone()= apply {visibility= View.GONE }

