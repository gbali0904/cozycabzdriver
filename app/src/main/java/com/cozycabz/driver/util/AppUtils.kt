package com.cozycabz.driver.util

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat

class AppUtils {


    companion object {
        /**
         * Checks Write to External Storage and Camera permission is provided or not.
         *
         * @param context
         * @return
         */
        fun isPermissionsGranted(context: Activity): Boolean {
            return !(Build.VERSION.SDK_INT > 22 && !(ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) === PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.CAMERA
            ) === PackageManager.PERMISSION_GRANTED))
        }
    }




}