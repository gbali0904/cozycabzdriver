package com.cozycabz.driver.login

import android.os.Bundle
import com.cozycabz.driver.R
import com.cozycabz.driver.di.base.BaseActivity
import com.cozycabz.driver.forgetpassword.ForgetPasswordFragment
import com.cozycabz.driver.home.HomeActivity
import com.cozycabz.driver.login.viewmodel.LoginViewModel
import com.cozycabz.driver.util.launchActivity
import com.cozycabz.driver.util.replaceFragment
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    private lateinit var baseviewModel: LoginViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        baseviewModel = viewModelFactory.create(LoginViewModel::class.java)
        login.setOnClickListener { launchActivity<HomeActivity>() }
        forgetPassword.setOnClickListener {
            replaceFragment(R.id.container, ForgetPasswordFragment(), true)
        }
    }
}
