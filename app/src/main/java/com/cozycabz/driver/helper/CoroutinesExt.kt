package com.cozycabz.driver.helper

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.concurrent.CancellationException

fun CoroutineScope.launchOrError(func: suspend CoroutineScope.() -> Unit,
                                 error: (error: Throwable) -> Unit): Job {

    return launch {
        try {
            func.invoke(this)
        }
        catch (throwable: Throwable) {
            if (throwable !is CancellationException){
                error.invoke(throwable)
            }
            Job()
        }
    }

}