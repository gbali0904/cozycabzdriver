package com.cozycabz.driver.splash

import android.Manifest
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Process
import android.provider.Settings
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.cozycabz.driver.R
import com.cozycabz.driver.login.LoginActivity
import com.cozycabz.driver.util.AppUtils
import com.cozycabz.driver.util.Constants
import com.cozycabz.driver.util.launchActivity
import java.util.*
import kotlin.concurrent.schedule
import kotlin.system.exitProcess

class SplashActivity : AppCompatActivity() {
    val EXTERNAL_PERM = 20
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        //Check for context for permission
        showContextForPermission()

    }


    /**
     * Explain the context why permission is needed. If Permission is already granted, reset the session.
     * Else ask for Permission.
     */
    private fun showContextForPermission() {
        if (!AppUtils.isPermissionsGranted(this)) {
            AlertDialog.Builder(this)
                .setMessage(Constants.PERMISSION_MESSAGE)
                .setPositiveButton(
                    "Ok"
                ) { _, _ ->
                    ActivityCompat.requestPermissions(
                        this@SplashActivity,
                        arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.CAMERA
                        ),
                        EXTERNAL_PERM
                    )
                }
                .setCancelable(false)
                .show()
        } else {

            handleLaunchIntent()

        }
    }

    private fun handleLaunchIntent() {
        Timer("SettingUp", false).schedule(500) {
            launchActivity<LoginActivity>()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == EXTERNAL_PERM) {
            if (grantResults.size == 4 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED
            ) {
                handleLaunchIntent()
            } else {

                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                ) {
                    Toast.makeText(this, "Permission was not granted.", Toast.LENGTH_SHORT).show()
                    //close the application
                    finish()
                    Process.killProcess(Process.myPid())
                    exitProcess(0)
                } else {
                    AlertDialog.Builder(this)
                        .setMessage(
                            "eActivation app cannot run because it has not been granted permission." +
                                    "\n\nStorage permission is required to function app smoothly.\n" +
                                    "\n" + "Go to Settings, and tap Permissions to grant permission."
                        )
                        .setPositiveButton("Settings") { dialog, which ->
                            dialog.dismiss()
                            goToSettings()
                        }
                        .setNegativeButton("Cancel") { dialog, which ->
                            dialog.dismiss()
                            finish()
                            Process.killProcess(Process.myPid())
                        }
                        .setCancelable(false)
                        .show()
                }

            }
        }

    }


    /**
     * Open settings of the App.
     */
    private fun goToSettings() {
        val myAppSettings = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        myAppSettings.data = Uri.parse("package:$packageName")
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT)
        myAppSettings.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(myAppSettings)
    }


}
