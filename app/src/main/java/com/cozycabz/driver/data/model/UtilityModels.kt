package com.airtel.cocp.data.model
data class GlobalState(val state: State, val message:String?=null)

enum class State(val code: Int, val message: String) {

    LOGOUT(555, "logout"),
    SESSION_EXPIRED(555,"SESSION EXPIRED"),
    ERROR(500,"Something went wrong. Please try again."),
    SUCCESS(200,"Success"),
    LOADING(1000,"Please wait a moment.")
}