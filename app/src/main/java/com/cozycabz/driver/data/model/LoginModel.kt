package com.cozycabz.driver.data.model

data class LoginModel(
    val code: Int,
    val error: Boolean,
    val login: Login,
    val msg: String
)

data class Login(
    val Mobile: String,
    val Name: String,
    val id: String,
    val status: String
)