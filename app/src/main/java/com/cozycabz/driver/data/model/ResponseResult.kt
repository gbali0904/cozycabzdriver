package com.cozycabz.driver.data.model

import org.json.JSONObject
sealed class ResponseResult<out R> {

    data class Success<out T>(val data: T) : ResponseResult<T>()
    data class Error(val code: Int? = null, val message: String?, val error: ErrorResponse? = null) : ResponseResult<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "${error?.message?:"Something went wrong. Please try again."}"
        }
    }
}

val ResponseResult<*>.succeeded
    get() = this is ResponseResult.Success && data != null

  fun <T> ResponseResult<T>.getResultData(): T{
      this as ResponseResult.Success<T>
      return data
  }

fun ResponseResult<*>.getErrorData(): Error{
    this as Error
    return this
}


data class ErrorResponse(val code: Int?, val message:String?, val data:JSONObject?=null)