package com.cozycabz.driver.data.remote.repo

import com.cozycabz.driver.data.model.ErrorResponse
import com.cozycabz.driver.data.model.ResponseResult
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.HttpException
import java.util.concurrent.CancellationException
import javax.inject.Inject
import kotlinx.coroutines.withContext as withContext1


abstract class BaseRepository {

    @Inject
    lateinit var gson: Gson

    protected val dispatcher: CoroutineDispatcher by lazy { Dispatchers.IO }

    suspend fun <T> executeApiCall(
        dispatcher: CoroutineDispatcher,
        apiCall: suspend () -> T
    ): ResponseResult<T> {
        return withContext1(dispatcher) {
            try {
                ResponseResult.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                when (throwable) {
                    is CancellationException -> throw CancellationException()
                    is HttpException -> {
                        val code = throwable.code()
                        val message = throwable.message
                        val errorResponse = throwable.response()?.errorBody()?.string()?.let {
                            gson.fromJson(it, ErrorResponse::class.java)
                        }
                        ResponseResult.Error(code, message, errorResponse)
                    }
                    else -> {
                        ResponseResult.Error(1111, "Something went wrong. Please try again.")
                    }
                }
            }
        }
    }

}