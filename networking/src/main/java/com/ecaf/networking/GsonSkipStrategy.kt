package com.ecaf.networking

import com.google.gson.ExclusionStrategy
import com.google.gson.FieldAttributes


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD)
internal annotation class JsonSkip

class GsonSkipStrategy: ExclusionStrategy {

    override fun shouldSkipField(f: FieldAttributes?): Boolean {
        return f?.getAnnotation(JsonSkip::class.java) != null
    }

    override fun shouldSkipClass(clazz: Class<*>?): Boolean {
        return false
    }
}