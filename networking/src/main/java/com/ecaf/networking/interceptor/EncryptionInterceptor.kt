package com.ecaf.networking.interceptor

import android.content.Context


open class EncryptionInterceptor constructor(private val context: Context, private val appName: String) : BaseInterceptor() {

    override fun createNewRequest(oldRequest: Request): Request {

        val newRequestBuilder = oldRequest.newBuilder()
        val oldRequestString = oldRequest.body().bodyToString()

        val encryptedString = NativeEncryptionUtil.encryptFromNative(context,
                oldRequestString,appName)

        val newRequestBody = RequestBody.create(
                MediaType.parse("text/plain; charset=utf-8"),
                encryptedString?:"")

//        LogUtils.debugLog(LogUtils.LOG_PREFIX, " URL for Request :: ${oldRequest.url()}")
//        LogUtils.debugLog(LogUtils.LOG_PREFIX, " requestBody before Encryption :: $oldRequestString")
//        LogUtils.debugLog(LogUtils.LOG_PREFIX, " finalRequestBody after Encryption :: $encryptedString")
        return newRequestBuilder.method(oldRequest.method(), newRequestBody).build()
    }
}