package com.ecaf.networking.interceptor

import com.ecaf.networking.RANDOM_TOKEN_MAP
import com.ecaf.networking.getReqHeaders
import java.util.*

class AuthInterceptor constructor(private val headersMap: Map<String,String>?) : BaseInterceptor() {

    override fun createNewRequest(oldRequest: Request): Request {
        val newRequestBuilder = oldRequest.newBuilder()
        val randomToken = UUID.randomUUID().toString()
        val requestBodyString = oldRequest.body().bodyToString()
        RANDOM_TOKEN_MAP[oldRequest.url().toString().hashCode().toString()] = randomToken
        val headerMap = getReqHeaders(oldRequest.url().toString(), requestBodyString, randomToken)
        headerMap.entries.forEach {
            newRequestBuilder.addHeader(it.key, it.value ?: "")
        }
        headersMap?.entries?.forEach{
            newRequestBuilder.header(it.key, it.value)
        }
        return newRequestBuilder.build()
    }
}