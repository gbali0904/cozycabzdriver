package com.ecaf.networking.interceptor

import android.content.Context
import android.text.TextUtils
import com.ecaf.networking.*

import org.json.JSONObject

open class DecryptionInterceptor constructor(private val applicationContext: Context, private val appName:String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())

            // Read Response Headers
            val respHeaders = mutableMapOf<String, String>()
            for (respHeaderEntry in response.headers().toMultimap().entries) {
                respHeaders[respHeaderEntry.key] = respHeaderEntry.value[respHeaderEntry.value.size - 1]
            }

            val responseString = response.body()?.string() ?: ""
            val decryptedString = getDecryptedJSON(applicationContext, appName, responseString)

            if (respHeaders.containsKey(ModuleConstants.RESPONSE_HASHCODE_HEADER)) {
                if (isResponseTampered(
                        RANDOM_TOKEN_MAP[chain.request().url().toString().hashCode().toString()]
                                ?: "",
                                respHeaders[ModuleConstants.RESPONSE_HASHCODE_HEADER].toString(),
                                decryptedString.hashCode().toString())
                )
                    throw RuntimeException("Response has been tampered. Please try again.")
            }

            val newResponse = response.newBuilder()
            var contentType = response.header(ModuleConstants.CONTENT_TYPE)
            if (TextUtils.isEmpty(contentType))
                contentType = ModuleConstants.TYPE_JSON

            val json: JSONObject
            try {
                json = isValidJson(decryptedString)
            } catch (e: Exception) {
                throw RuntimeException("Response is not a valid JSON String")
            }
            //In case of server error
            if (!response.isSuccessful) {
                try {
                    val errorMessage = json.getJSONObject("status").getString("message")
                    newResponse.message(errorMessage)
                } catch (exception: Exception) {
                    exception.printStackTrace()
                }

            }

            newResponse.body(ResponseBody.create(MediaType.parse(contentType
                    ?: ModuleConstants.TYPE_JSON), decryptedString))
            return newResponse.build()
    }
}