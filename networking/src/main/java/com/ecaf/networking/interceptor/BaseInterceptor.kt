package com.ecaf.networking.interceptor


abstract class BaseInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(createNewRequest(chain.request()))
    }

    abstract fun createNewRequest(oldRequest: Request): Request
}