package com.ecaf.networking

import android.content.Context
import android.text.TextUtils
import org.json.JSONObject
import java.lang.RuntimeException
import java.util.*


    fun getReqHeaders(url: String, requestBody: String, randomToken: String): Map<String, String?> {
        val headerMap = mutableMapOf<String, String?>()
        headerMap[ModuleConstants.CONTENT_LANGUAGE_KEY] = Locale.getDefault().language
        headerMap[ModuleConstants.CONTENT_TYPE] = ModuleConstants.TYPE_JSON
        headerMap[ModuleConstants.USER_AGENT_KEY] = ModuleConstants.USER_AGENT_VALUE

        if (!TextUtils.isEmpty(requestBody))
            headerMap[ModuleConstants.REQUEST_HASHCODE_HEADER] = SecurityApp.encrypt(requestBody.hashCode().toString(),
                ModuleConstants.KEY)
        headerMap[ModuleConstants.REQUEST_RANDOM_TOKEN_HEADER] = getEncryptedRandomToken(randomToken)

//        LogUtils.debugLog(LogUtils.LOG_PREFIX, "Headers Count is: " + headerMap.size)
//        LogUtils.debugLog(LogUtils.LOG_PREFIX, "Headers values are::  " +
//                headerMap[ModuleConstants.JWT_TOKEN_HEADER_KEY] + " :: securityField :: " +
//                headerMap[ModuleConstants.REQUEST_RANDOM_TOKEN_HEADER])
        return headerMap
    }

    fun getEncryptedRandomToken(randomToken: String): String? {
        val formatRandomToken = randomToken + ModuleConstants.DELIMITER + System.currentTimeMillis()
        return SecurityApp.encrypt(formatRandomToken, ModuleConstants.KEY)
    }

    fun RequestBody?.bodyToString(): String {
        if (this == null) return ""
        val buffer = okio.Buffer()
        writeTo(buffer)
        return buffer.readUtf8()
    }

    fun isResponseTampered(randomToken: String, encryptedHashCode: String, responseHashCode: String): Boolean {
        try {
            val decryptHashCode = SecurityApp.decrypt(encryptedHashCode, randomToken)

            if (!responseHashCode.equals(decryptHashCode!!, ignoreCase = true)) {
                return true
            }
        } catch (e: Exception) {
            return true
        }

        return false
    }

    fun getDecryptedJSON(applicationContext: Context, appName: String, text: String): String {
        return if (text == null && text.trim().isEmpty()) {
            throw RuntimeException("Invalid Response String")
        } else {
            val decryptedResponse: String = NativeEncryptionUtil.decryptFromNative(applicationContext,
                text, appName)
            decryptedResponse
        }
    }

    fun isValidJson(decryptedString: String): JSONObject {
        return JSONObject(decryptedString)
    }

    val RANDOM_TOKEN_MAP = mutableMapOf<String, String>()