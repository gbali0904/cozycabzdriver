package com.ecaf.networking

import android.util.Base64
import android.util.Log
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.nio.charset.Charset
import javax.crypto.Cipher
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.PBEParameterSpec

object SecurityApp {
    // For Logging
    private val CLASS_NAME = "SecurityCrypt"
    private val showClassLog = false

    fun encrypt(str: String, passPhrase: String): String? {
        val dcipher: Cipher
        val ecipher: Cipher
        val salt = byteArrayOf(0xA8.toByte(), 0x9B.toByte(), 0xC8.toByte(), 0x32.toByte(), 0x56.toByte(), 0x34.toByte(), 0xE3.toByte(), 0x03.toByte())

        val iterationCount = 19

        try {
            val keySpec = PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount)
            val key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec)

            ecipher = Cipher.getInstance(key.algorithm)
            dcipher = Cipher.getInstance(key.algorithm)

            val paramSpec = PBEParameterSpec(salt, iterationCount)

            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec)
            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec)

            //  actual encryption
            val utf8 = str.toByteArray(charset("UTF8"))
            val enc = ecipher.doFinal(utf8)
            return Base64.encodeToString(enc,Base64.NO_WRAP).trim { it <= ' ' }
        } catch (e: Exception) {
            Log.e("Exception", "$CLASS_NAME >> encrypt >> Exception: $e", e)
        }

        return null
    }

    fun decrypt(str: String, passPhrase: String): String? {
        val dcipher: Cipher
        val ecipher: Cipher
        val salt = byteArrayOf(0xA8.toByte(), 0x9B.toByte(), 0xC8.toByte(), 0x32.toByte(), 0x56.toByte(), 0x34.toByte(), 0xE3.toByte(), 0x03.toByte())

        val iterationCount = 19

        try {
            val keySpec = PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount)
            val key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec)

            ecipher = Cipher.getInstance(key.algorithm)
            dcipher = Cipher.getInstance(key.algorithm)

            val paramSpec = PBEParameterSpec(salt, iterationCount)

            ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec)
            dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec)

            // actual decryption
            //            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);
            val dec = Base64.decode(str.trim { it <= ' ' },Base64.NO_WRAP)
            val utf8 = dcipher.doFinal(dec)

            return String(utf8, Charset.defaultCharset())
        } catch (e: Exception) {
            Log.e("Exception", "$CLASS_NAME >> encrypt >> Exception: $e", e)
        }

        return null
    }

    @Throws(IOException::class)
    fun getBytes(input: InputStream): ByteArray {
        val output = ByteArrayOutputStream()
        copy(input, output)
        return output.toByteArray()
    }

    @Throws(IOException::class)
    fun copy(input: InputStream, output: OutputStream): Int {
        val count = copyLarge(input, output)
        return if (count > 2147483647L) -1 else count.toInt()
    }

    @Throws(IOException::class)
    @JvmOverloads
    fun copyLarge(input: InputStream, output: OutputStream, buffer: ByteArray = ByteArray(4096)): Long {
        var count = 0L

        var n1 = input.read(buffer)
        while (-1 != n1) {
            output.write(buffer, 0, n1)
            count += n1.toLong()
            n1 = input.read(buffer)
        }

        return count
    }


}