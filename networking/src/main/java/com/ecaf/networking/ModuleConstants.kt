package com.ecaf.networking
object ModuleConstants {

    const val CONTENT_TYPE = "Content-Type"
    const val TYPE_JSON = "application/json;charset=UTF-8"
    const val USER_AGENT_KEY = "User-Agent"
    const val CONTENT_LANGUAGE_KEY = "content-language"
    const val USER_AGENT_VALUE = "android"
    const val REQUEST_HASHCODE_HEADER = "request_signature"
    const val KEY = "968754ABCP8R8"
    const val DELIMITER = "$$"
    const val REQUEST_RANDOM_TOKEN_HEADER = "securityField"
    const val RESPONSE_HASHCODE_HEADER = "signature"

}