package com.ecaf.networking

import android.content.Context
import com.ecaf.networking.interceptor.AuthInterceptor
import com.ecaf.networking.interceptor.DecryptionInterceptor
import com.ecaf.networking.interceptor.EncryptionInterceptor
import java.util.concurrent.TimeUnit


class RetrofitClient(builder: Builder) {

    private var debugEnabled: Boolean = false
    private var requestEncryptionEnabled: Boolean = false
    private var responseEncryptionEnabled: Boolean = false
    private var baseUrl: String
    private var headers: Map<String, String>? = null
    private var context: Context
    private var appName: String
    private var connectTimeout: Long
    private var readTimeout: Long
    private var writeTimeout: Long
    private var converterFactory: Converter.Factory?
    private var encryptionInterceptors: Array<Interceptor>?
    private var interceptors: Array<Interceptor>?

    init {
        context = builder.context
        debugEnabled = builder.debugEnabled
        requestEncryptionEnabled = builder.requestEncryptionEnabled
        responseEncryptionEnabled = builder.responseEncryptionEnabled
        baseUrl = builder.baseUrl
        headers = builder.headers
        appName = builder.appName
        connectTimeout = builder.connectTimeout
        readTimeout = builder.readTimeout
        writeTimeout = builder.writeTimeout
        converterFactory = builder.converterFactory
        encryptionInterceptors = builder.encryptionInterceptors
        interceptors = builder.interceptors
    }


    fun <T> create(service: Class<T>): T {
        if (baseUrl.isNullOrBlank()) throw Exception("Base Url should not be empty.")
        var okHttpBuilder = OkHttpClient.Builder()
        okHttpBuilder.connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
            .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
            .writeTimeout(writeTimeout, TimeUnit.MILLISECONDS)
        if (debugEnabled)
            okHttpBuilder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))

        okHttpBuilder.addInterceptor(AuthInterceptor(headers))
        interceptors?.let { interceptors ->
            for (interceptor in interceptors) {
                okHttpBuilder.addInterceptor(interceptor)
            }
        }
        if (encryptionInterceptors != null) {
            for (interceptor in encryptionInterceptors!!) {
                okHttpBuilder.addInterceptor(interceptor)
            }
        } else {
            if (requestEncryptionEnabled)
                okHttpBuilder.addInterceptor(EncryptionInterceptor(context, appName))
            if (responseEncryptionEnabled)
                okHttpBuilder.addInterceptor(DecryptionInterceptor(context, appName))
        }
        return Retrofit.Builder().client(okHttpBuilder.build())
            .baseUrl(baseUrl)
            .addConverterFactory(converterFactory ?: GsonConverterFactory.create())
            .build()
            .create(service)
    }

    class Builder(val context: Context) {

        internal var debugEnabled: Boolean = false
        internal var requestEncryptionEnabled: Boolean = false
        internal var responseEncryptionEnabled: Boolean = false
        internal lateinit var baseUrl: String
        internal var headers: Map<String, String>? = null
        internal var appName: String = "ECAF"
        internal var connectTimeout: Long = 10_000
        internal var writeTimeout: Long = 20_000
        internal var readTimeout: Long = 20_000
        internal var converterFactory: Converter.Factory? = null
        internal var encryptionInterceptors: Array<Interceptor>? = null
        internal var interceptors: Array<Interceptor>? = null

        fun setAppName(appName: String): Builder =
            apply { this.appName = appName }

        fun setDebug(enable: Boolean): Builder = apply { debugEnabled = enable }

        fun setDefaultRequestEncryption(enable: Boolean): Builder =
            apply { requestEncryptionEnabled = enable }

        fun setDefaultResponseEncryption(enable: Boolean): Builder =
            apply { responseEncryptionEnabled = enable }

        fun setExtraHeaders(headers: Map<String, String>): Builder =
            apply { this.headers = headers }

        fun baseUrl(baseUrl: String) = apply { this.baseUrl = baseUrl }

        fun setConnectTimeout(connectTimeout: Long): Builder =
            apply { this.connectTimeout = connectTimeout }

        fun setReadTimeout(readTimeout: Long): Builder = apply { this.readTimeout = readTimeout }

        fun setWriteTimeout(writeTimeout: Long): Builder =
            apply { this.writeTimeout = writeTimeout }

        fun setConverterFactory(converterFactory: Converter.Factory): Builder =
            apply { this.converterFactory = converterFactory }

        fun setEncryptionInterceptors(interceptors: Array<Interceptor>) =
            apply { this.encryptionInterceptors = interceptors }

        fun setInterceptors(interceptors: Array<Interceptor>) =
            apply { this.interceptors = interceptors }

        fun build(): RetrofitClient {
            return RetrofitClient(this)
        }
    }
}